﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Controllers;
using Abp.ObjectMapping;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SweetShop_2.Controllers;
using SweetShop_2.Customer;
using SweetShop_2.Customer.Dto;
using SweetShop_2.Order;
using SweetShop_2.Order.Dto;
using SweetShop_2.Products;
using SweetShop_2.Products.Dto;
using SweetShop_2.Web.Models.Order;

namespace SweetShop_2.Web.Mvc.Controllers
{
    public class OrderController : AbpController
    {
        private readonly ICustomerServices customerServices;
        private readonly IOrderServices orderServices;
        private readonly IProductServices productServices;
        private readonly IObjectMapper mapper;

        public OrderController(ICustomerServices customerServices,IOrderServices orderServices
            , IProductServices productServices,IObjectMapper mapper)
        {
            this.customerServices = customerServices;
            this.orderServices = orderServices;
            this.productServices = productServices;
            this.mapper = mapper;
        }
        [HttpGet]
        public IActionResult AddOrder()
        {
            AddOrderViewModel model = new AddOrderViewModel
            {
                ProductList = new List<SelectListItem>()
            };
            var products = productServices.GetProducts();
            model.GetProName = mapper.Map<List<ProductDto>>(products);
            model.GetCusName = mapper.Map<List<CustomerDto>>(customerServices.GetCustomers());
            return View(model);
        }
        [HttpPost]
        public IActionResult SaveOrder([FromBody]OrderViewModel model)
        {
            var order = mapper.Map<OrderDto>(model);
            orderServices.SaveOrder(order);
            return RedirectToAction("Order", "AddOrder");
        }
        public IActionResult OrderList()
        {
            OrderViewModel model = new OrderViewModel();
            model.order = mapper.Map<List<OrderDto>>(orderServices.GetAllOrders());
            return View(model);
        }
        public IActionResult OrderDetails(int id)
        {
            var model = mapper.Map<OrderVM>(orderServices.GetOrderById(id));
            return View(model);
        }
        [HttpGet]
        public IActionResult UpdateOrder(int id)
        {
            var model = mapper.Map<UpdateOrderViewModel>(orderServices.GetOrderById(id));
            return View(model);
        }
        [HttpPost]
        public IActionResult UpdateOrder([FromBody]UpdateOrderViewModel updateOrder)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var updatedOrder = mapper.Map<OrderDto>(updateOrder);
                    orderServices.UpdateOrder(updatedOrder);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return NotFound();
        }
        public IActionResult DeleteOrder(int id)
        {
            orderServices.DeleteOrder(id);
            return RedirectToAction("ItemList", "AddOrder");
        }
    }
}