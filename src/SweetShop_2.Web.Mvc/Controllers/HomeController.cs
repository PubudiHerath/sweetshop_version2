﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using SweetShop_2.Controllers;

namespace SweetShop_2.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : SweetShop_2ControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
