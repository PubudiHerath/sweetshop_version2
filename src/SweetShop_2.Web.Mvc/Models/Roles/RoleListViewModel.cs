﻿using System.Collections.Generic;
using SweetShop_2.Roles.Dto;

namespace SweetShop_2.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleListDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
