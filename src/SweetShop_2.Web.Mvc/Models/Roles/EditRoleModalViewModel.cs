﻿using Abp.AutoMapper;
using SweetShop_2.Roles.Dto;
using SweetShop_2.Web.Models.Common;

namespace SweetShop_2.Web.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class EditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool HasPermission(FlatPermissionDto permission)
        {
            return GrantedPermissionNames.Contains(permission.Name);
        }
    }
}
