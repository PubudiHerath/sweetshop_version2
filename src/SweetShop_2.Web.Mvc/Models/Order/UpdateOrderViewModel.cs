﻿using SweetShop_2.Order.Dto;
using SweetShop_2.Products.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop_2.Web.Models.Order
{
    public class UpdateOrderViewModel
    {
        public int customerId { get; set; }
        public int OrderId { get; set; }
        public int productid { get; set; }
        public DateTime OrderDate { get; set; }
        public ProductDto product { get; set; }
        public OrderDto order { get; set; }
        public List<OrderItemDto> orderItems { get; set; }
    }
}
