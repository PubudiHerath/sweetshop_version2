﻿using Microsoft.AspNetCore.Mvc.Rendering;
using SweetShop_2.Customer.Dto;
using SweetShop_2.Products.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop_2.Web.Models.Order
{
    public class AddOrderViewModel
    {

        [Required]
        public List<SelectListItem> ProductList { get; set; }
        public int GetProtId { get; set; }
        [Required]
        public int CustomerId { get; set; }
        [Required]
        public List<CustomerDto> GetCusName { get; set; }
        [Required]
        public List<ProductDto> GetProName { get; set; }
        public string ProDescription { get; set; }
        public DateTime OrderDate { get; set; }
        [Required]
        public int Quantity { get; set; }
        public int UnitPrice { get; set; }
    }
}
