﻿using SweetShop_2.Order;
using SweetShop_2.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop_2.Web.Models.Order
{
    public class OrderItemsVM
    {
        [Key]
        public int OrderItemId { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
        public Product product { get; set; }
        public Orders order { get; set; }
        public bool IsDeleted { get; set; }
    }
}
