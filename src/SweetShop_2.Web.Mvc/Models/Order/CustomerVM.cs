﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop_2.Web.Models.Order
{
    public class CustomerVM
    {
        [Key]
        public int CusId { get; set; }
        [Required]
        public string CusName { get; set; }
        [Required]
        public string CusAddress { get; set; }
        [Required]
        [Range(1, 10)]
        public int CusContactNumber { get; set; }
    }
}
