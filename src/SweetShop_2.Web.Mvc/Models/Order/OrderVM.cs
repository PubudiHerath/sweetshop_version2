﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop_2.Web.Models.Order
{
    public class OrderVM
    {

        [Key]
        public int OrderId { get; set; } = 0;
        public DateTime OrderDate { get; set; }
        [Required]
        public List<OrderItemsVM> orderItems { get; set; }
        [Required]
        public int customerId { get; set; }
        [ForeignKey(nameof(customerId))]
        public virtual CustomerVM customer { get; set; }
    }
}
