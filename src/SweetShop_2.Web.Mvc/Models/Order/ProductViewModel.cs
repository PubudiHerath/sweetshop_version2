﻿using SweetShop_2.Products.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop_2.Web.Models.Order
{
    public class ProductViewModel
    {
        public List<ProductDto> ProNameList { get; set; }
    }
}
