﻿using SweetShop_2.Order.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop_2.Web.Models.Order
{
    public class OrderViewModel
    {
        [Required]
        public int customerId { get; set; }
        public DateTime OrderDate { get; set; }
        [Required]
        public List<OrderItemDto> orderItems { get; set; }
        public List<OrderDto> order { get; set; }
        public string customerName { get; set; }
    }
}
