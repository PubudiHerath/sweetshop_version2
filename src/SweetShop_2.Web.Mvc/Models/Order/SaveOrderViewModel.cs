﻿using SweetShop_2.Customer.Dto;
using SweetShop_2.Products.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SweetShop_2.Web.Models.Order
{
    public class SaveOrderViewModel
    {
        public int ProductId { get; set; }
        [Required]
        public int CustomerId { get; set; }
        public List<ProductDto> Products { get; set; }
        public List<CustomerDto> Customers { get; set; }
        [Required]
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        [Required]
        public int Quantity { get; set; }
        public int UnitPrice { get; set; }
        public string OrderDate { get; set; }
    }
}
