using System.Collections.Generic;
using SweetShop_2.Roles.Dto;
using SweetShop_2.Users.Dto;

namespace SweetShop_2.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
