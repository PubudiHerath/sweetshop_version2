﻿using System.Collections.Generic;
using SweetShop_2.Roles.Dto;

namespace SweetShop_2.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}