﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace SweetShop_2.Web.Views
{
    public abstract class SweetShop_2RazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected SweetShop_2RazorPage()
        {
            LocalizationSourceName = SweetShop_2Consts.LocalizationSourceName;
        }
    }
}
