﻿using SweetShop_2.Configuration.Ui;

namespace SweetShop_2.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
