﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace SweetShop_2.Web.Views
{
    public abstract class SweetShop_2ViewComponent : AbpViewComponent
    {
        protected SweetShop_2ViewComponent()
        {
            LocalizationSourceName = SweetShop_2Consts.LocalizationSourceName;
        }
    }
}
