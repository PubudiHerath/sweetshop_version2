﻿using SweetShop_2.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SweetShop_2.EntityFrameworkCore.Seed.Customer
{
    public class DefaultCustomerBuilder
    {
        private readonly SweetShop_2DbContext context;

        public DefaultCustomerBuilder(SweetShop_2DbContext context)
        {
            this.context = context;
        }
        public void Create()
        {
            CreateDefaultCustomer();
        }

        private void CreateDefaultCustomer()
        {
            if(!context.Customers.Any())
            {
                var defaultCustomers = new List<Customers>
                {
                    new Customers{CusName="Customer1", CusAddress="Colombo", CusContactNumber= 0711223571},
                    new Customers{CusName="Customer2", CusAddress="Kandy", CusContactNumber= 0751223571},
                     new Customers{CusName="Customer3", CusAddress="Galle", CusContactNumber= 0761223571},
                      new Customers{CusName="Customer4", CusAddress="Kurunegala", CusContactNumber= 0771223571}
                };
                context.Customers.AddRange(defaultCustomers);
            }
        }
    }
}
