﻿using SweetShop_2.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SweetShop_2.EntityFrameworkCore.Seed.Products
{
    public class DefaultProductBuilder
    {
        private readonly SweetShop_2DbContext context;

        public DefaultProductBuilder(SweetShop_2DbContext context)
        {
            this.context = context;
        }
        public void Create()
        {
            CreateDefaultProduct();
        }

        private void CreateDefaultProduct()
        {
            if (!context.Customers.Any())
            {
                var defaultProducts = new List<Product>
                {
                    new Product{ProductName="Cake", ProductDescription="This is cake", Price= 100, Quantity=1000},
                    new Product{ProductName="Chocolate", ProductDescription="This is chocolate", Price= 50, Quantity=1000},
                     new Product{ProductName="Muffin", ProductDescription="This is muffin", Price= 150, Quantity=1000},
                      new Product{ProductName="Toffee", ProductDescription="This is toffee", Price= 10, Quantity=1000}
                };
                context.Products.AddRange(defaultProducts);
            }
        }
    }
}
