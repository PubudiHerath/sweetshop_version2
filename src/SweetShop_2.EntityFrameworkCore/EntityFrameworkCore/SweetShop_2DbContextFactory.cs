﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using SweetShop_2.Configuration;
using SweetShop_2.Web;

namespace SweetShop_2.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class SweetShop_2DbContextFactory : IDesignTimeDbContextFactory<SweetShop_2DbContext>
    {
        public SweetShop_2DbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<SweetShop_2DbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            SweetShop_2DbContextConfigurer.Configure(builder, configuration.GetConnectionString(SweetShop_2Consts.ConnectionStringName));

            return new SweetShop_2DbContext(builder.Options);
        }
    }
}
