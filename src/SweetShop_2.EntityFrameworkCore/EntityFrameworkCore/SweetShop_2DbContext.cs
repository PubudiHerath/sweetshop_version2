﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using SweetShop_2.Authorization.Roles;
using SweetShop_2.Authorization.Users;
using SweetShop_2.MultiTenancy;
using SweetShop_2.Products;
using SweetShop_2.Customer;
using SweetShop_2.Order;

namespace SweetShop_2.EntityFrameworkCore
{
    public class SweetShop_2DbContext : AbpZeroDbContext<Tenant, Role, User, SweetShop_2DbContext>
    {

        public DbSet<Product> Products { get; set; }
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<OrderItems> OrderItems { get; set; }

        public SweetShop_2DbContext(DbContextOptions<SweetShop_2DbContext> options)
            : base(options)
        {
        }
    }
}
