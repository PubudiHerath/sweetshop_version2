using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace SweetShop_2.EntityFrameworkCore
{
    public static class SweetShop_2DbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<SweetShop_2DbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<SweetShop_2DbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
