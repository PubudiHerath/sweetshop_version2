﻿using Abp.Application.Services;
using SweetShop_2.Order.Dto;
using SweetShop_2.Products.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop_2.Products
{
    public interface IProductServices:IApplicationService
    {
        List<ProductDto> GetProducts();
        ProductDto  GetProductById(int id);
        void UpdateQuantityInStock(List<OrderItemDto> orderItems);
    }
}
