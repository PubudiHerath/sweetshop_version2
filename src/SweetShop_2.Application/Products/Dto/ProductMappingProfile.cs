﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop_2.Products.Dto
{
    public class ProductMappingProfile: Profile
    {
        public ProductMappingProfile()
        {
            CreateMap<ProductDto, Product>().ReverseMap();
        }
    }
}
