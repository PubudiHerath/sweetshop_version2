﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Microsoft.EntityFrameworkCore;
using SweetShop_2.Order;
using SweetShop_2.Order.Dto;
using SweetShop_2.Products.Dto;

namespace SweetShop_2.Products
{
    public class ProductServices : IProductServices
    {
        private readonly IRepository<Product> productRepository;
        private readonly IRepository<OrderItems> orderItemRepository;
        private readonly IObjectMapper mapper;

        public ProductServices(IRepository<Product> productRepository, 
            IRepository<OrderItems> OrderItemRepository, IObjectMapper mapper)
        {
            this.productRepository = productRepository;
            orderItemRepository = OrderItemRepository;
            this.mapper = mapper;
        }
        public ProductDto GetProductById(int id)
        {
            var product = productRepository.Get(id);
            return mapper.Map<ProductDto>(product);
        }

        public List<ProductDto> GetProducts()
        {
            var products = productRepository.GetAll();
            return mapper.Map<List<ProductDto>>(products);
        }

        public void UpdateQuantityInStock(List<OrderItemDto> orderItems)
        {
            foreach (var item in orderItems)
            {
                var products = productRepository.Get(item.product.ProductId);
                var orderItem = orderItemRepository.GetAll().AsNoTracking()
                    .FirstOrDefault(o => o.OrderItemId == item.OrderItemId);
                var orderQuantity = orderItem.Quantity;
                var orderedQuantity = item.Quantity;
                var quantityInHand = products.Quantity;
                var quantityToUpdate = 0;
                var newQuantity = 0;

                if (orderedQuantity < orderQuantity)
                {
                    quantityToUpdate = orderQuantity - orderedQuantity;
                    newQuantity = quantityInHand + quantityToUpdate;
                }
                else if (orderedQuantity > orderQuantity)
                {
                    quantityToUpdate = orderedQuantity - orderQuantity;
                    newQuantity = quantityInHand - quantityToUpdate;
                }
                else if (orderedQuantity == orderQuantity)
                {
                    newQuantity = quantityInHand - orderedQuantity;
                }
                //if (quantityInHand <= 0)
                //{
                //    throw new NoProductsInStockException();
                //}
                products.Quantity = newQuantity;
                productRepository.Update(products);
               
            }
        }
    }
}
