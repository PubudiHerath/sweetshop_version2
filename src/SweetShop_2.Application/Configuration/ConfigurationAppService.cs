﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using SweetShop_2.Configuration.Dto;

namespace SweetShop_2.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : SweetShop_2AppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
