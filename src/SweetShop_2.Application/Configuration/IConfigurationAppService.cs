﻿using System.Threading.Tasks;
using SweetShop_2.Configuration.Dto;

namespace SweetShop_2.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
