﻿using System.ComponentModel.DataAnnotations;

namespace SweetShop_2.Configuration.Dto
{
    public class ChangeUiThemeInput
    {
        [Required]
        [StringLength(32)]
        public string Theme { get; set; }
    }
}
