﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SweetShop_2.MultiTenancy.Dto;

namespace SweetShop_2.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

