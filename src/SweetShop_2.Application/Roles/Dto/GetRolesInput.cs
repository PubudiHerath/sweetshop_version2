﻿namespace SweetShop_2.Roles.Dto
{
    public class GetRolesInput
    {
        public string Permission { get; set; }
    }
}
