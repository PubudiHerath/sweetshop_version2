﻿using Abp.Application.Services.Dto;

namespace SweetShop_2.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

