﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using SweetShop_2.Authorization;

namespace SweetShop_2
{
    [DependsOn(
        typeof(SweetShop_2CoreModule), 
        typeof(AbpAutoMapperModule))]
    public class SweetShop_2ApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<SweetShop_2AuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(SweetShop_2ApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
