﻿using Abp.Application.Services;
using SweetShop_2.Customer.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop_2.Customer
{
    public interface ICustomerServices:IApplicationService
    {
        List<CustomerDto> GetCustomers();
        CustomerDto GetCustomerById(int id);
    }
}
