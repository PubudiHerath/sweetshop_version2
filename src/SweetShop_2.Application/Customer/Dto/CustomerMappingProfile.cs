﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;


namespace SweetShop_2.Customer.Dto
{
   public class CustomerMappingProfile:Profile
    {
        public CustomerMappingProfile()
        {
            CreateMap<CustomerDto, Customers>().ReverseMap();
           
        }

    }
}
