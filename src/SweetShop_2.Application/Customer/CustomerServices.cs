﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using SweetShop_2.Customer.Dto;

namespace SweetShop_2.Customer
{
    public class CustomerServices : ICustomerServices
    {
        private readonly IRepository<Customers> customerRepository;
        private readonly IObjectMapper mapper;

        public CustomerServices(IRepository<Customers> customerRepository,IObjectMapper mapper )
        {
            this.customerRepository = customerRepository;
            this.mapper = mapper;
        }
        public CustomerDto GetCustomerById(int id)
        {
            var customer = customerRepository.Get(id);
            return mapper.Map<CustomerDto>(customer);
        }

        public List<CustomerDto> GetCustomers()
        {
            var customers = customerRepository.GetAll().ToList();
            return mapper.Map<List<CustomerDto>>(customers);
        }
    }
}
