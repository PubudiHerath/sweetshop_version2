﻿using Abp.Application.Services;
using SweetShop_2.Order.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop_2.Order
{
    public interface IOrderServices:IApplicationService
    {
        void SaveOrder(OrderDto order);
        List<OrderDto> GetAllOrders();
        OrderDto GetOrderById(int id);
        void UpdateOrder(OrderDto orderItems);
        void DeleteOrder(int id);
    }
}
