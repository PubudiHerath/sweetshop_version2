﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;


namespace SweetShop_2.Order.Dto
{
    public class OrderMappingProfile:Profile
    {
        public OrderMappingProfile()
        {
            CreateMap<OrderDto, Orders>().ReverseMap();
            CreateMap<OrderItemDto, OrderItems>().ReverseMap();
         

        }
    }
}
