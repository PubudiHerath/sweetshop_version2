﻿using SweetShop_2.Customer;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop_2.Order.Dto
{
    public class OrderDto
    {
        public int OrderId { get; set; }
        public int OrderItemId { get; set; }
        public DateTime OrderDate { get; set; }
        public Customers customer { get; set; }
        public List<OrderItemDto> orderItems { get; set; }
        public int customerId { get; set; }
    }
}
