﻿using SweetShop_2.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop_2.Order.Dto
{
    public class OrderItemDto
    {
        public int OrderItemId { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
        public Product product { get; set; }
        public bool IsDeleted { get; set; }
    }
}
