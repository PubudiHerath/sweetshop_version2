﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.ObjectMapping;
using Microsoft.EntityFrameworkCore;
using SweetShop_2.Customer;
using SweetShop_2.Order.Dto;
using SweetShop_2.Products;

namespace SweetShop_2.Order
{
    public class OrderServices : IOrderServices
    {
        private readonly IRepository<Product> productRepository;
        private readonly IRepository<Customers> customerRepository;
        private readonly IObjectMapper mapper;
        private readonly IUnitOfWorkManager unitOfWorkManager;
        private readonly IRepository<Orders> orderRepository;
        private readonly IProductServices productServices;
        private readonly IRepository<OrderItems> orderItemRepository;

        public OrderServices(IRepository<Product> productRepository,
            IRepository<Customers> customerRepository, IObjectMapper mapper, 
            IUnitOfWorkManager unitOfWorkManager, IRepository<Orders> orderRepository,
            IProductServices productServices, IRepository<OrderItems> orderItemRepository)
        {
            this.productRepository = productRepository;
            this.customerRepository = customerRepository;
            this.mapper = mapper;
            this.unitOfWorkManager = unitOfWorkManager;
            this.orderRepository = orderRepository;
            this.productServices = productServices;
            this.orderItemRepository = orderItemRepository;
        }
        public void DeleteOrder(int id)
        {
            var orderToDelete = orderRepository.GetAll().AsNoTracking().Include(o => o.orderItems)
                                                 .FirstOrDefault(f => f.Id == id);
            if (orderToDelete == null)
            {
                throw new OrderNotFoundException();
            }

            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                try
                {
                    productServices.UpdateQuantityInStock(mapper.
                        Map<List<OrderItemDto>>(orderToDelete.orderItems));
                    orderRepository.Delete(id);
                    unitOfWork.Complete();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public List<OrderDto> GetAllOrders()
        {
            var orders = orderRepository.GetAll().Include(i => i.orderItems)
                .ThenInclude(p => p.product).ToList();
            return mapper.Map<List<OrderDto>>(orders);
        }

        public OrderDto GetOrderById(int id)
        {
            var order = orderRepository.Get(id);
            return mapper.Map<OrderDto>(order);
        }

        public void SaveOrder(OrderDto order)
        {
            if (order == null)
            {
                throw new OrderNotFoundException();
            }
            using (var unitOfWork = unitOfWorkManager.Begin())
            {
                try
                {
                    foreach (var item in order.orderItems)
                    {
                       
                        var id = item.ProductId;
                        var orderitem = productServices.GetProductById(id);
                        var price = Convert.ToInt32(orderitem.Price);
                        item.Price = price;
                    }
                    
                    productServices.UpdateQuantityInStock(order.orderItems);
                    var OrderNew = mapper.Map<Orders>(order);
                    orderRepository.Insert(OrderNew);
                    unitOfWork.Complete();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

            public void UpdateOrder(OrderDto orderItems)
            {
                if (orderItems == null)
                {
                    throw new OrderNotFoundException();
                }
                else
                {
                    foreach (var items in orderItems.orderItems)
                    {
                        if (items == null)
                        {
                            throw new OrderItemNotFoundException();
                        }
                    }
                }
                using (var unitOfWork = unitOfWorkManager.Begin())
                {
                    try
                    {
                        List<OrderItemDto> updateItemList = new List<OrderItemDto>();
                        foreach (var items in orderItems.orderItems)
                        {
                            if (items.IsDeleted == true)
                            {
                                var ToBeDeletedId = items.OrderItemId;
                                var item = orderItemRepository.GetAll()
                                        .AsNoTracking().Include(o => o.order).Include(p => p.product)
                                        .FirstOrDefault(i => i.Id == ToBeDeletedId);
                                orderItemRepository.Delete(ToBeDeletedId);
                            }
                            if (items.IsDeleted == false)
                            {
                                var ToBeUpdatedId = items.ProductId;
                                var item = productRepository.GetAll()
                                  .AsNoTracking().FirstOrDefault(i => i.Id == ToBeUpdatedId);
                                items.Price = Convert.ToInt32(item.Price);
                                updateItemList.Add(items);
                            }
                        }
                        orderItems.orderItems = updateItemList;
                        orderRepository.Update(mapper.Map<Orders>(orderItems));
                        unitOfWork.Complete();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

            }
    }
}
