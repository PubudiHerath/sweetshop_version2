﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using SweetShop_2.MultiTenancy;

namespace SweetShop_2.Sessions.Dto
{
    [AutoMapFrom(typeof(Tenant))]
    public class TenantLoginInfoDto : EntityDto
    {
        public string TenancyName { get; set; }

        public string Name { get; set; }
    }
}
