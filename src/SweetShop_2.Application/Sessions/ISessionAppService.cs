﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SweetShop_2.Sessions.Dto;

namespace SweetShop_2.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
