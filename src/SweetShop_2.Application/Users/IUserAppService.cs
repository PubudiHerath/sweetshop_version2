using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SweetShop_2.Roles.Dto;
using SweetShop_2.Users.Dto;

namespace SweetShop_2.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
