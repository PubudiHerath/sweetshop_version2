using System.ComponentModel.DataAnnotations;

namespace SweetShop_2.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}