﻿using Abp.Authorization;
using SweetShop_2.Authorization.Roles;
using SweetShop_2.Authorization.Users;

namespace SweetShop_2.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
