﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace SweetShop_2.Localization
{
    public static class SweetShop_2LocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SweetShop_2Consts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SweetShop_2LocalizationConfigurer).GetAssembly(),
                        "SweetShop_2.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
