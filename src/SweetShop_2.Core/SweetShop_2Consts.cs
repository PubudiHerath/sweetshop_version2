﻿namespace SweetShop_2
{
    public class SweetShop_2Consts
    {
        public const string LocalizationSourceName = "SweetShop_2";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
