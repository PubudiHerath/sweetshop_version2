﻿using Abp.MultiTenancy;
using SweetShop_2.Authorization.Users;

namespace SweetShop_2.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
