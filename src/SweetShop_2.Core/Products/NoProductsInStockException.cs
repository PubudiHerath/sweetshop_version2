﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SweetShop_2.Products
{
    public class NoProductsInStockException:Exception
    {
        public NoProductsInStockException() : base("No Products are available in the stock!")
        {

        }
    }
}
