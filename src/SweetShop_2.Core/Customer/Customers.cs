﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SweetShop_2.Customer
{
    public class Customers: Entity <int>
    {
        [Key]
        public int CusId { get; set; }
        [Required]
        public string CusName { get; set; }
        [Required]
        public string CusAddress { get; set; }
        [Required]
        [Range(1, 10)]
        public int CusContactNumber { get; set; }
    }
}
