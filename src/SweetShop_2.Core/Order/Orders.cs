﻿using Abp.Domain.Entities;
using SweetShop_2.Customer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SweetShop_2.Order
{
    public class Orders : Entity
    {
        [Key]
        public int OrderId { get; set; } = 0;
        public DateTime OrderDate { get; set; }
        [Required]
        public List<OrderItems> orderItems { get; set; }
        [Required]
        public int customerId { get; set; }
        [ForeignKey(nameof(customerId))]
        public virtual Customers customer { get; set; }
    }
}
