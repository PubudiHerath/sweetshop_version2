﻿using Abp.AutoMapper;
using SweetShop_2.Authentication.External;

namespace SweetShop_2.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
