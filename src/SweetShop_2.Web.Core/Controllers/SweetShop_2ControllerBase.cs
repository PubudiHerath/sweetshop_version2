using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace SweetShop_2.Controllers
{
    public abstract class SweetShop_2ControllerBase: AbpController
    {
        protected SweetShop_2ControllerBase()
        {
            LocalizationSourceName = SweetShop_2Consts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
