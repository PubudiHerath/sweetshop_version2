using Microsoft.AspNetCore.Antiforgery;
using SweetShop_2.Controllers;

namespace SweetShop_2.Web.Host.Controllers
{
    public class AntiForgeryController : SweetShop_2ControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
