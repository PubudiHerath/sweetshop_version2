﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using SweetShop_2.Configuration;

namespace SweetShop_2.Web.Host.Startup
{
    [DependsOn(
       typeof(SweetShop_2WebCoreModule))]
    public class SweetShop_2WebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public SweetShop_2WebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SweetShop_2WebHostModule).GetAssembly());
        }
    }
}
